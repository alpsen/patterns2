```plantuml
@startuml myDiagram
scale 2
skinparam ClassAttributeIconSize 0

interface IProduct {
+GetName(): string
+GetPrice(): double
+Accept(visitor: IVisitor): void
}

interface IVisitor {
+Visit(product: IProduct): void
}

class Product {
-name: string
-price: double
+Product(name: string, price: double)
+GetName(): string
+GetPrice(): double
+Accept(visitor: IVisitor): void
}

class Customer {
+Visit(product: IProduct): void
}

abstract class ProductDecorator {
-product: IProduct
+ProductDecorator(product: IProduct)
+GetName(): string
+GetPrice(): double
+Accept(visitor: IVisitor): void
}

class DiscountProduct {
-discount: double
+DiscountProduct(product: IProduct, discount: double)
+GetName(): string
+GetPrice(): double
+Accept(visitor: IVisitor): void
}

IProduct <|.. Product
IProduct <|.. ProductDecorator
IVisitor <|.. Customer
ProductDecorator <|-- DiscountProduct
@enduml
```
