﻿using System;
using System.Collections.Generic;

interface IProduct
{
    string GetName();
    double GetPrice();
    void Accept(IVisitor visitor);
}

class Product : IProduct
{
    private string name;
    private double price;

    public Product(string name, double price)
    {
        this.name = name;
        this.price = price;
    }

    public string GetName()
    {
        return name;
    }

    public double GetPrice()
    {
        return price;
    }

    public void Accept(IVisitor visitor)
    {
        visitor.Visit(this);
    }
}

 interface IVisitor
{
    abstract  void Visit(IProduct product);
}

class Customer : IVisitor
{
    public void Visit(IProduct product)
    {
        Console.WriteLine("Покупатель купил " + product.GetName() + " за " + product.GetPrice() + "р");
    }
}

abstract class ProductDecorator : IProduct
{
    protected IProduct product;

    public ProductDecorator(IProduct product)
    {
        this.product = product;
    }

    public virtual string GetName()
    {
        return product.GetName();
    }

    public virtual double GetPrice()
    {
        return product.GetPrice();
    }

    public virtual void Accept(IVisitor visitor)
    {
        visitor.Visit(this);
    }
}

class DiscountProduct : ProductDecorator
{
    private double discount;

    public DiscountProduct(IProduct product, double discount) : base(product)
    {
        this.discount = discount;
    }

    public override string GetName()
    {
        return this.product.GetName();
    }

    public override double GetPrice()
    {
        return this.product.GetPrice() * (1 - discount / 100); 
    }

    public override void Accept(IVisitor visitor)
    {
        Console.WriteLine("DiscountProduct " + GetName()+ " за "+ product.GetPrice() + "р" + 
            " с " + discount + "% скидкой");
        base.Accept(visitor);
    }
}

class Program
{
    static void Main(string[] args)
    {
        List<IProduct> products = new List<IProduct>();
        products.Add(new Product("iPhone", 50000));
        products.Add(new DiscountProduct(new Product("MacBook", 90000), 10));

        Customer customer = new Customer();

        foreach (var product in products)
        {
            product.Accept(customer);
        }

        Console.ReadKey();
    }
}